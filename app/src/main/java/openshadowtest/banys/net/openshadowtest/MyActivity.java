package openshadowtest.banys.net.openshadowtest;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import openshadowtest.banys.net.openshadowtest.adapters.EmployeesAdapter;
import openshadowtest.banys.net.openshadowtest.loaders.EmployeesLoader;
import openshadowtest.banys.net.openshadowtest.model.Employee;


public class MyActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<List<Employee>>{

    private static final String TAG = MyActivity.class.getSimpleName();

    @InjectView(R.id.gridView)
    GridView mGridView;

    @InjectView(R.id.progressBar)
    ProgressBar mProgressBar;

    @InjectView(R.id.tv_info)
    TextView mInfo;

    private EmployeesAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        ButterKnife.inject(this);
        getSupportLoaderManager().initLoader(0, null, this).forceLoad(); // forceLoad because of problems in support library
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Employee employee = (Employee)mAdapter.getItem(i);
                startActivity(PhotoActivity.getIntent(MyActivity.this, employee));
            }
        });
    }


    /*****
     * LoaderCallbacks methods
     ****/

    @Override
    public Loader<List<Employee>> onCreateLoader(int i, Bundle bundle) {
        return new EmployeesLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<Employee>> listLoader, List<Employee> employees) {
        Log.v(TAG, "onLoadFinished, size: " + employees.size());
        mAdapter = new EmployeesAdapter(this, employees);
        mGridView.setAdapter(mAdapter);
        if (!employees.isEmpty()) {
            // if not empty we should show gridview
            mProgressBar.setVisibility(View.GONE);
            mInfo.setVisibility(View.GONE);
            mGridView.setVisibility(View.VISIBLE);
        } else {
            // if empty we should show info
            mProgressBar.setVisibility(View.GONE);
            mInfo.setVisibility(View.VISIBLE);
            mGridView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Employee>> listLoader) {
        mAdapter.setData(null);
        mProgressBar.setVisibility(View.VISIBLE);
        mInfo.setVisibility(View.GONE);
        mGridView.setVisibility(View.GONE);
    }
}
