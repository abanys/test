package openshadowtest.banys.net.openshadowtest.model;

import java.util.List;

/**
 * Created by banan on 27.08.14.
 */
public class ResponseHolder {

    private List<Employee> list;

    public ResponseHolder() { }

    public List<Employee> getList() {
        return list;
    }

    public void setList(List<Employee> list) {
        this.list = list;
    }
}
