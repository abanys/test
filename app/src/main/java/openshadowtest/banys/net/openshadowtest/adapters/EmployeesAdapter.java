package openshadowtest.banys.net.openshadowtest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import openshadowtest.banys.net.openshadowtest.R;
import openshadowtest.banys.net.openshadowtest.model.Employee;

/**
 * Created by banan on 27.08.14.
 */
public class EmployeesAdapter extends BaseAdapter {

    private List<Employee> mEmployees;
    private LayoutInflater mInflater;
    private DisplayImageOptions mOptions;

    public EmployeesAdapter(Context ctx, List<Employee> employees) {
        mEmployees = employees;
        mInflater = LayoutInflater.from(ctx);
        mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.placeholder)
                .showImageOnFail(R.drawable.error)
                .build();
    }

    @Override
    public int getCount() {
        return mEmployees.size();
    }

    @Override
    public Object getItem(int i) {
        return mEmployees.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.valueOf(mEmployees.get(i).getId());
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(R.layout.grid_item_employee, null);
            ViewHolder holder = new ViewHolder();
            holder.photo = (ImageView)view.findViewById(R.id.photo);
            view.setTag(holder);
        }
        ViewHolder holder = (ViewHolder)view.getTag();
        Employee employee = mEmployees.get(i);
        ImageLoader.getInstance().displayImage(employee.getPicture(), holder.photo, mOptions);
        return view;
    }

    static class ViewHolder {
        ImageView photo;
    }

    public void setData(List<Employee> employees) {
        if (employees == null)
            mEmployees = new ArrayList<Employee>();
        else
            mEmployees = new ArrayList<Employee>(employees);
    }
}
