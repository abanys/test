package openshadowtest.banys.net.openshadowtest.net;

/**
 * Created by banan on 04.06.14.
 */
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RequestManager {

    private HttpClient httpClient;

    /**
     * initialize httpClient if not exists, if exists just returns it
     * @return initialized HttpClient
     * @throws org.apache.http.client.ClientProtocolException
     * @throws java.io.IOException
     */
    private HttpClient getHttpClient() throws ClientProtocolException, IOException {
        if (httpClient != null)
            return httpClient;
        HttpParams httpParameters = new BasicHttpParams();
        int timeoutConnection = 10000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = 15000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        httpClient = new DefaultHttpClient(httpParameters);
        return httpClient;
    }


    /**
     * method executes given query and returns server response InputStreamReader
     *
     * @param query
     * @return server reply as String
     * @throws org.apache.http.client.ClientProtocolException
     * @throws java.io.IOException
     */
    public InputStreamReader executeISR(String query) throws ClientProtocolException, IOException {
        Log.d(getClass().getSimpleName(), "url: " + query);
        HttpClient httpClient = getHttpClient();
        HttpGet httpGet = new HttpGet(query);

        HttpResponse response = httpClient.execute(httpGet);
        if(response == null){
            throw new IOException();
        }
        return new InputStreamReader(response.getEntity().getContent());
    }

    /**
     * method executes given query and returns server response InputStream
     */
    public InputStream executeIS(String query) throws ClientProtocolException, IOException {
        Log.d(getClass().getSimpleName(), "url: " + query);
        HttpClient httpClient = getHttpClient();
        HttpGet httpGet = new HttpGet(query);

        HttpResponse response = httpClient.execute(httpGet);
        if(response == null){
            throw new IOException();
        }
        return response.getEntity().getContent();
    }

}
