package openshadowtest.banys.net.openshadowtest;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.ButterKnife;
import butterknife.InjectView;
import openshadowtest.banys.net.openshadowtest.model.Employee;


public class PhotoActivity extends ActionBarActivity {

    private static final String ARG_EMPLOYEE = "arg_employee";

    private Employee mEmployee;
    private ShareActionProvider mShareActionProvider;

    @InjectView(R.id.photo)
    ImageView mPhoto;

    @InjectView(R.id.tv_info)
    TextView mInfo;

    public static Intent getIntent(Context ctx, Employee employee) {
        Intent intent = new Intent(ctx, PhotoActivity.class);
        intent.putExtra(ARG_EMPLOYEE, employee);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        ButterKnife.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent() != null) {
            mEmployee = (Employee)getIntent().getSerializableExtra(ARG_EMPLOYEE);
        }
        if (mEmployee != null)
            setupView();

    }

    private void setupView() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .showImageOnLoading(R.drawable.placeholder)
                .showImageOnFail(R.drawable.error)
                .build();
        ImageLoader.getInstance().displayImage(mEmployee.getPicture(), mPhoto, options);
        mInfo.setText(mEmployee.getName() + ": " + mEmployee.getDescription() );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_share) {
            share();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void share() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, mEmployee.getPicture());
        shareIntent.setType("text/plain");
        startActivity(Intent.createChooser(shareIntent, ""));
    }

}
