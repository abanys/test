package openshadowtest.banys.net.openshadowtest.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import openshadowtest.banys.net.openshadowtest.configuration.Configuration;
import openshadowtest.banys.net.openshadowtest.model.Employee;
import openshadowtest.banys.net.openshadowtest.model.ResponseHolder;
import openshadowtest.banys.net.openshadowtest.net.RequestManager;

/**
 * Created by banan on 27.08.14.
 */
public class EmployeesLoader extends AsyncTaskLoader<List<Employee>> {

    private static final String TAG = EmployeesLoader.class.getSimpleName();

    private List<Employee> mEmployees;

    public EmployeesLoader(Context context) {
        super(context);
    }

    @Override
    public List<Employee> loadInBackground() {
        Log.v(TAG, "loadInBackground");
        RequestManager requestManager = new RequestManager();
        List<Employee> employees = new ArrayList<Employee>();
        try {
            JsonReader reader = new JsonReader(new InputStreamReader(
                    requestManager.executeIS(Configuration.API_EMPLOYEES_URL)));  //get list from net
            ResponseHolder responseHolder = new Gson().fromJson(reader, ResponseHolder.class);
            employees = responseHolder.getList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.v(TAG, "will return size: " + employees.size());
        return employees;
    }


    @Override
    protected void onStartLoading() {
        if (mEmployees != null)
            deliverResult(mEmployees);
    }

}
