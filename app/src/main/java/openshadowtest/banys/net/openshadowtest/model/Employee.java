package openshadowtest.banys.net.openshadowtest.model;

import java.io.Serializable;

/**
 * Created by banan on 27.08.14.
 */
public class Employee implements Serializable {

    private String id;
    private String name;
    private String description;
    private String picture;
    private String hired;

    public Employee() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getHired() {
        return hired;
    }

    public void setHired(String hired) {
        this.hired = hired;
    }
}
